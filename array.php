<!doctype html>
<html>
    <head>
    <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
    </head>

<body>
  <?php
    $x = array("a" => "red", "b" => "green");
    $y = array("c" => "blue", "d" => "yellow");
    
    print_r($x + $y);
    #retun union of $x and $y
    ?>
    <br>
    <br>
   <?php
    $x = array("a" => "red", "b" => "green");
    $y = array("c" => "blue", "d" => "yellow");
    
    var_dump($x == $y);
    ?>
    
    <br>
    <br>
    <?php
    $x = array("a" => "red", "b" => "yellow");
    $y = array("c" => "blue", "d" => "yellow");
    
    var_dump($x === $y);
    ?>
    
<br>
    <br>
    
    <?php
    
    $x = array("a" => "red", "b" => "green");
    $y = array("c" => "blue", "d" => "yellow");
    
    var_dump($x != $y);
    ?>
    <br>
    <br>
    <?php 
    $x = array("a" => "red", "b" => "green");
    $y = array("c" => "blue", "d" => "yellow");
    
    var_dump($x <> $y);
    ?>
    
    <br>
    <br>
    
    <?php 
    $x = array("a" => "red", "b" => "green");
    $y = array("c" => "blue", "d" => "yellow");
    
    var_dump($x !== $y);
    ?>
    <p>The PHP array operators are used to compare arrays.</p>
    <table>
    <tr>
        <th>Operator</th>
        <th>Name</th>
        <th>Example</th>
        <th>Result</th>
        </tr>
        
        <tr>
        <td>+</td>
            <td>Union</td>
            <td>$x + $y</td>
            <td>Union of $x and $y</td>
        </tr>
    
        <tr>
        <td>==</td>
            <td>Equality</td>
            <td>$x == $y</td>
            <td>Returns true if $x and $y have the same key/value pairs</td>
        </tr>
        
        <tr>
        <td>===</td>
            <td>	Identity</td>
            <td>$x === $y</td>
            <td>Returns true if $x and $y have the same key/value pairs in the same order and of the same types</td>
        </tr>
        
        <tr>
        <td>!=</td>
            <td>Inequality</td>
            <td>$x != $y</td>
            <td>Returns true if $x is not equal to $y</td>
        </tr>
        
        <tr>
        <td><></td>
            <td>Inequality</td>
            <td>$x <> $y</td>
            <td>Returns true if $x is not equal to $y</td>
        </tr>
        
        <tr>
        <td>!==</td>
            <td>Non-identity</td>
            <td>$x !== $y	</td>
            <td>Returns true if $x is not identical to $y</td>
        </tr>
        
        
    </table>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    </body>












</html>