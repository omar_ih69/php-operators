<!doctype html>
<html>
<head>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
    
    
    
</head>
<body>
    
   <?php
    $x =100;
    $y ="100";
    
    var_dump($x == $y);
    //returns true because values are equal
    ?> 
    
    <?php
    $x = 100;
    $y = "100";
    
    var_dump($x === $y);
    //returns false because type are not equal
    ?>
    
    <?php
$x = 100;  
$y = "100";

var_dump($x != $y);
    // returns false because values are equal
?> 
    <?php
    $x = 100;
    $y = "100";
    
    var_dump($x <> $y);
    //returns false because values are equal
    ?>
    
    <?php 
    $x = 100;
    $y ="100";
    
    var_dump($x !== $y);
    //returns true because types are not equal
    ?>
    
    <?php
$x = 100;
$y = 50;

var_dump($x > $y); // returns true because $x is greater than $y
?>
  <?php
$x = 10;
$y = 50;

var_dump($x < $y); // returns true because $x is less than $y
?>  
    
    <?php
$x = 50;
$y = 50;

var_dump($x >= $y); // returns true because $x is greater than or equal to $y
?> 
    
 <?php
$x = 50;
$y = 50;

var_dump($x <= $y); // returns true because $x is less than or equal to $y
?>  

  <table>
    
     <tr>
        <th>Operator</th>
        <th>Name</th>
        <th>Example</th>
         <th>Result</th>
        </tr>
    
    <tr>
      <td>==</td>
        <td>Equal</td>
        <td>$x == $y</td>
        <td>Returns true if $x is equal to $y</td>
      </tr>
      
      <tr>
      <td>===</td>
          <td>Identical</td>
          <td>$x === $y</td>
          <td>Returns true if $x is equal to $y, and they are of the same type</td>    
      
      </tr>
    <tr>
      
      <td>!=</td>
        <td>Not Equal</td>
      <td>$x != $y</td>
      <td>Returns true if $x is not equal to $y</td>
      </tr>
    
      <tr>
      <td><></td>
      <td>Not Equal</td>
<td>$x <> $y</td>   
          <td>Returns true if $x is not equal to $y</td>
          
      </tr>
      
      <tr>
      <td>!==</td>
          <td>Not identical</td>
          <td>$x !== $y</td>
          <td>Returns true if $x is not equal to $y, or they are not of the same type</td>
      </tr>
      
      <tr>
      
      <td>></td>
          <td>Greater Than</td>
          <td>$x > $y</td>
          <td>Returns true if $x is greater than $y</td>
      </tr>
      <tr>
      
      <td><</td>
     <td>Less than</td> 
    <td>$x < $y</td>
      <td>Returns true if $x is less than $y</td>
      </tr>
      
      <tr>
      
      <td>>=</td>
     <td>Greater than or equal to</td> 
    <td>$x >= $y</td>
      <td>Returns true if $x is greater than or equal to $</td>
      </tr>
      
      <tr>
      
      <td><=</td>
     <td>Less than or equal to</td> 
    <td>$x <= $y</td>
      <td>Returns true if $x is less than or equal to $y</td>
      </tr>
      
    </table>  
       
    
    </body>

</html>