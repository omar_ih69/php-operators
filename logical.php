<!doctype html>
<html>
<head>
    
    <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
    </head>
<body>
    <?php
    $x = 100;
    $y = 50;
    
    if($x == 100 and $y == 50){
        echo "hello world";
    }
    ?>
    
    <?php
    $x = 100;
    $y = 50;
    
    if($x == 100 or $y == 80){
        echo "hello world";
    }
    
    ?>
   
    <?php
    $x = 100;
    $y =50;
    
    if($x == 100 xor $y == 80){
        echo "hello world";
    }
    ?>
    
    <?php 
    $x = 100;
    $y = 50;
    
    if($x == 100 && $y == 50){
        echo "hello world";
    }
    ?>
    <?php
$x = 100;  
$y = 50;

if ($x == 100 || $y == 80) {
    echo "Hello world!";
}
?>  

    <?php
$x = 100;  

if ($x !== 90) {
    echo "Hello world!";
}
?>
    
    <table>
    <tr>
        <th>Operator</th>
        <th>Name</th>
        <th>Example</th>
        <th>Result</th>
        </tr>
        
        <tr>
        <td>and</td>
            <td>And</td>
            <td>$x and $y</td>
            <td>True if both $x and $y are true</td>
        
            </tr>
    
        
        <tr>
        <td>or</td>
            <td>Or</td>
            <td>$x or $y</td>
            <td>True if either $x or $y is true</td>
        
            </tr>
        
        
        <tr>
        <td>xor</td>
            <td>Xor</td>
            <td>	$x xor $y</td>
            <td>True if either $x or $y is true, but not both</td>
        
            </tr>
        
        <tr>
        <td>&&</td>
            <td>And</td>
            <td>$x && $y</td>
            <td>True if both $x and $y are true</td>
        
            </tr>
    
    
    
        <tr>
        <td>||</td>
            <td>Or</td>
            <td>	$x || $y</td>
            <td>True if either $x or $y is true</td>
        
    
        </tr>
    
        <tr>
        <td>!</td>
            <td>Not</td>
            <td>!$x</td>
            <td>True if $x is not true</td>
        
        </tr>
    
    
    </table>
    
    
    </body>

</html>