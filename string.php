<!doctype html>
<html>
<head>
    
    <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
    </head>
<body>
    
    <?php 
    
    $txt1 = "hello";
    $txt2 = " world";
    
    echo $txt1 . $txt2;
    ?>
    
    <?php
    $txt = "hello";
    $txt2 = " world";
    $txt .= $txt2;
    echo $txt;
    ?>
    
    <table>
    <tr>
        <th>Operator</th>
        <th>Name</th>
        <th>Example</th>
        <th>Result</th>
        
        </tr>
    <tr>
        <td>.</td>
        <td>Concatenation</td>
        <td>$txt1 . $txt2</td>
        <td>Concatenation of $txt1 and $txt2</td>
        
        
        </tr>
    
    <tr>
        <td>.=</td>
        <td>	Concatenation assignment</td>
        <td>$txt1 .= $txt2</td>
        <td>Appends $txt2 to $txt1</td>
        
        
        </tr>
    
    </table>
    
    </body>







</html>