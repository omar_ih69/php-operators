<!doctype html>
<html>

<head>
     <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
    
    </head>

<body>
   <?php 
    $x = 10;
    echo ++$x;
    ?>
    
    
    
    <?php
    
    $x = 10;
    echo $x++;
    ?>
    
    <?php
    $x = 10;
    echo --$x;
    ?>
    
    <?php
    
    $x = 10;
    echo $x--;
    ?>
    
    <table>
    <tr>
        <th>Operator</th>
        <th>Name</th>
        <th>Description</th>
        </tr>
    <tr>
        <td>++$x</td>
        <td>Pre-increment</td>
        <td>Increments $x by one, then returns $x</td>    
        
        </tr>
    <tr>
        <td>$x++</td>
        <td>Post-increment</td>
        <td>Returns $x, then increments $x by one</td>
        </tr>
        
        <tr>
        
        <td>--$x</td>
        <td>Pre-decrement</td>
        <td>Decrements $x by one, then returns $x</td>
        </tr>
        
        <tr>
        <td>$x--</td>
        <td>Post-decrement</td>
            <td>Returns $x, then decrements $x by one</td>
        </tr>
    </table>
    
    
    
    
    </body>





</html>